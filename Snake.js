var direction = 0;
var sizeG;
var pions;
var eat;
var pos;
var tick;
var speed;
var score;
var lastDir;
var tickG;
function setup() {
  createCanvas(600, 600);
  pions = [];
  eat = [];
  sizeG = 10;
  speed = 3;
  pos = {
         x : int(random(width)/sizeG),
         y : int(random(height)/sizeG)
  }
  tick = 0;
  tickG = 0;
  score = 1;
  
}

function draw() {
  if (tick > speed) {
    tickG++;
    if (tickG > 2){
      tickG = 0;
      
      var lose = false;
      var post = null;
      while (!lose) {
        post = {
         x : int(random(width)/sizeG),
         y : int(random(height)/sizeG)
        };
        lose = true;
        pions.forEach(function(p) {
          if (p.x == post.x && p.y == post.y){
            lose = false;
          }
        });
      }
      eat.push(post);
    }
    lastDir = direction;
    if (score > 0) {
      if (pions.length >= score) {
        pions.shift();
      }
      pions.push(({x : pos.x, y : pos.y}));
    }
    switch (direction) {
      case 0:
        pos.x++;
        break;
      case 1:
        pos.y--;
        break;
      case 2:
        pos.x--;
        break;
      case 3:
        pos.y++;
        break;
      default:
        break;
    }
    var lose = false;
    pions.forEach(function(p) {
      if (p.x == pos.x && p.y == pos.y){
        lose = true;
      }
    });
    if (pos.x < 0 || pos.y < 0 || pos.x*sizeG > width || pos.y*sizeG > height || lose) {
      
      pions = [];
      eat = [];
      sizeG = 10;
      speed = 3;
      pos = {
             x : int(random(width)/sizeG),
             y : int(random(height)/sizeG)
      }
      tick = 0;
      tickG = 0;
      score = 1;
    }
    var pose = null;
    eat.forEach(function(p) {
      if (p.x == pos.x && p.y == pos.y){
        pose = p;
      }
    });
    if (pose != null){
      eat = eat.filter(function(value, index, arr) {
        return value.x != pose.x || value.y != pose.y;
      });
      score++;
    }
    background(51);
    fill(255);
    rect(pos.x*sizeG, pos.y*sizeG, sizeG, sizeG);
    pions.forEach(function(p) {
    rect(p.x*sizeG, p.y*sizeG, sizeG, sizeG);
    });
    fill(255, 0, 0);
    eat.forEach(function(p) {
    rect(p.x*sizeG, p.y*sizeG, sizeG, sizeG);
    });
    tick = 0;
  }
  tick++;
}


function keyPressed() {
  if (keyCode === UP_ARROW && lastDir != 3) {
    direction = 1;
  } else if (keyCode === DOWN_ARROW && lastDir != 1) {
    direction = 3;
  } else if (keyCode === RIGHT_ARROW && lastDir != 2) {
    direction = 0;
  } else if (keyCode === LEFT_ARROW && lastDir != 0) {
    direction = 2;
  }
}
